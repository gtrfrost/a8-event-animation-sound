This scene aims to acheive the required features requested by Carl Fravel for Build in Decentraland course:  Assignment 8

The requirements are:

- Create a new scene
- Create a new GitLab project to hold it
- Consider adding a LICENSE or LICENSE.md file to your scene, to indicate what your licensing is for others using your scene.
- Make any or all of the following kinds of changes from the DCL-Tech sample scene library 
    -Which is located at https://gitlab.com/dcl-tech and https://github.com/decentraland/decentraland-ecs-utils

- Add or modify code to cause changes to happen in your scene as a result of clicking on something
- Add or modify code to cause something to rotate or move with the passage of time
- Add to your scene one or more gltf or glb files that contain animation clips, and launch a clip when something in the scene is clicked on.  Consider using a Mixamo animated character.
- Add a sound to your scene that is triggered by clicking on something.
- Add a collider to a primitive
- Using Blender, add a collider to a model that doesn’t already have one, export it to glb, and use it in your scene.
- Create a tinted glass, or a partially transparent piece of mesh like foliage
- Provide, in the class channel, a link to your gitlab repository, and at least one of the following ways of demonstrating your scene and its dynamic behavior
- Create a video that demonstrates your walking through the scene, perhaps with voice over.  OBS is a great tool for making such videos.  And you can then upload the video to YouTube or Vimeo, or put it in some cloud service available to the class.  Provide a link to the video.
- Put the scene on land, provide the explorer link to the land
- Put the scene into the Zeit Now service, provide a link to it there


NOTE: I have added colliders but have removed them from the scene as they caused the player to get stuck.