import {spawnGltfX} from './modules/SpawnerFunctions'
import utils from "../node_modules/decentraland-ecs-utils/index"
import { InterpolationType } from "../node_modules/decentraland-ecs-utils/transform/math/interpolation"

/*
let tSphere = new Entity()
engine.addEntity(tSphere)
tSphere.addComponent(new GLTFShape("models/triangle_sphere1.gltf"))
*/
let tSphereShape = new GLTFShape("models/triangle_sphere1.gltf")
let tSphere = spawnGltfX(tSphereShape, 8,1.8,6.1,  -180,34.031,-180,  .2,.2,.2)    

let error_ringShape = new GLTFShape("models/cyber_warning_ring/cyber_warning_ring.glb")
let error_ring = spawnGltfX(error_ringShape, 8,1.8,6.1,  -180,34.031,-180,  .005,.005,.005)    



let transparent_animationShape = new GLTFShape("models/block animation/transparent_animation2.gltf")
let transparent_animation = spawnGltfX(transparent_animationShape, 8,2.2,6.1,  0,0,0,  .1,.1,.1)    

const animator = new Animator();
let cubeMove = new AnimationState("cubeAnimation")
animator.addClip(cubeMove);

transparent_animation.addComponent(animator);

cubeMove.play()

/*
let error_collider_ringShape = new GLTFShape("models/cyber_warning_ring/cyber_warning_ring_collider.glb")
let error_collider_ring = spawnGltfX(error_collider_ringShape, 8,1.8,6.1,  -180,34.031,-180,  .005,.005,.005)    
*/

/*
let tsphereTransform = new Transform()
tSphere.addComponent(tsphereTransform)
*/
let deflatedScale = new Vector3(0.2, 0.2, 0.2)
let inflatedScale = new Vector3(5, 5, 5)

let errordeflatedScale = new Vector3(.005, .005, .005)
let errorinflatedScale = new Vector3(.05, .05, .05)


/*
TODO: get animated colliders to PUSH not engulf the player which freezes their movement
let errorColliderDeflatedScale = new Vector3(.005, .005, .005)
let errorColliderInflatedScale = new Vector3(.1, .1, .1)
*/

//tsphereTransform.position.set(8, 1.8, 6.1)
//tsphereTransform.scale = deflatedScale

let isInflating = false

let deflatedSound = new AudioClip("sounds/Speech On.wav")

tSphere.addComponent(new AudioSource(deflatedSound))

tSphere.addComponent(new OnClick(
	e => {
		if (isInflating) return
    isInflating = true
    tSphere.getComponent(AudioSource).playOnce()
		tSphere.addComponent(new utils.ScaleTransformComponent(
			deflatedScale,
			inflatedScale,
			.5,
			null,
			InterpolationType.EASEINQUAD
    ))
		tSphere.addComponent(new utils.Delay(3000,
			() => {
				
				tSphere.addComponent(new utils.ScaleTransformComponent(
					inflatedScale,
					deflatedScale,
					1,
					() => {
            isInflating = false
					}
        ))
        error_ring.addComponent(new utils.ScaleTransformComponent(
          errordeflatedScale,
          errorinflatedScale,
          1,
          null,
          InterpolationType.EASEINQUAD
        ))
        /*
        error_collider_ring.addComponent(new utils.ScaleTransformComponent(
          errorColliderDeflatedScale,
          errorColliderInflatedScale,
          1,
          null,
          InterpolationType.EASEINQUAD
        ))
        */
			}
		))
	}
))
/*
let ground = new Entity()
engine.addEntity(ground)
ground.addComponent(new GLTFShape("models/FloorBaseDirt_01.glb"))
ground.addComponent(new Transform({
	position: new Vector3(8, 0, 8)
}))
*/
const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
//engine.addEntity(scene)



const myText = new TextShape("Access Denied")

myText.fontSize = 50
myText.color = Color3.Blue()

scene.addComponent(myText)

class RotatorSystem {
  update(dt: number) {
   // rotate particular object(s)
   tSphere.getComponent(Transform).rotate(Vector3.Up(), dt * 60)
   error_ring.getComponent(Transform).rotate(Vector3.Up(), dt * 60) 
  }
}

// Add a new instance of the system to the engine
engine.addSystem(new RotatorSystem())

/// --- Spawner function ---

/*
function spawnCube(x: number, y: number, z: number,sx: number, sy: number, sz: number) {
  // create the entity
  const cube = new Entity()

  // add a transform to the entity
  cube.addComponent(new Transform({ 
      position: new Vector3(x, y, z),
      scale: new Vector3(sx, sy, sz)
  }))

  // add a shape to the entity
  cube.addComponent(new BoxShape())
 
  // add the entity to the engine
  engine.addEntity(cube)


  return cube
}
*/

/// --- Spawn a triangle ---
/*
const triangleSphere = spawnTriangleSphere(8, 1.6, 6.1, 0.1,0.1,0.1)
function spawnTriangleSphere(x: number, y: number, z: number,sx: number, sy: number, sz: number) {
  // create the entity
  const triangleSphere = new Entity()

  // add a transform to the entity
  triangleSphere.addComponent(new Transform({ 
      position: new Vector3(x, y, z),
      scale: new Vector3(sx, sy, sz)
  }))

  const gltfDimond = new GLTFShape('models/triangle_sphere/triangle_sphere.glb')
  // add a shape to the entity
  triangleSphere.addComponentOrReplace(gltfDimond)
 
  // add the entity to the engine
  engine.addEntity(triangleSphere)


  return triangleSphere
}
*/



const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseGrass_01)

const floorHexa_01 = new Entity()
floorHexa_01.setParent(scene)
const gltfShape_2 = new GLTFShape('models/FloorHexa_01/FloorHexa_01.glb')
floorHexa_01.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(13, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01.addComponentOrReplace(transform_3)
engine.addEntity(floorHexa_01)

const floorHexa_01_2 = new Entity()
floorHexa_01_2.setParent(scene)
floorHexa_01_2.addComponentOrReplace(gltfShape_2)
const transform_4 = new Transform({
  position: new Vector3(2, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_2.addComponentOrReplace(transform_4)
engine.addEntity(floorHexa_01_2)

const floorHexa_01_3 = new Entity()
floorHexa_01_3.setParent(scene)
floorHexa_01_3.addComponentOrReplace(gltfShape_2)
const transform_5 = new Transform({
  position: new Vector3(3, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_3.addComponentOrReplace(transform_5)
engine.addEntity(floorHexa_01_3)

const floorHexa_01_4 = new Entity()
floorHexa_01_4.setParent(scene)
floorHexa_01_4.addComponentOrReplace(gltfShape_2)
const transform_6 = new Transform({
  position: new Vector3(5, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_4.addComponentOrReplace(transform_6)
engine.addEntity(floorHexa_01_4)

const floorHexa_01_5 = new Entity()
floorHexa_01_5.setParent(scene)
floorHexa_01_5.addComponentOrReplace(gltfShape_2)
const transform_7 = new Transform({
  position: new Vector3(7, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_5.addComponentOrReplace(transform_7)
engine.addEntity(floorHexa_01_5)

const floorHexa_01_6 = new Entity()
floorHexa_01_6.setParent(scene)
floorHexa_01_6.addComponentOrReplace(gltfShape_2)
const transform_8 = new Transform({
  position: new Vector3(9, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_6.addComponentOrReplace(transform_8)
engine.addEntity(floorHexa_01_6)

const floorHexa_01_7 = new Entity()
floorHexa_01_7.setParent(scene)
floorHexa_01_7.addComponentOrReplace(gltfShape_2)
const transform_9 = new Transform({
  position: new Vector3(11, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_7.addComponentOrReplace(transform_9)
engine.addEntity(floorHexa_01_7)

const floorHexa_01_8 = new Entity()
floorHexa_01_8.setParent(scene)
floorHexa_01_8.addComponentOrReplace(gltfShape_2)
const transform_10 = new Transform({
  position: new Vector3(15, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_8.addComponentOrReplace(transform_10)
engine.addEntity(floorHexa_01_8)

const floorHexa_01_9 = new Entity()
floorHexa_01_9.setParent(scene)
floorHexa_01_9.addComponentOrReplace(gltfShape_2)
const transform_11 = new Transform({
  position: new Vector3(1, 0, 1),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_9.addComponentOrReplace(transform_11)
engine.addEntity(floorHexa_01_9)

const floorHexa_01_10 = new Entity()
floorHexa_01_10.setParent(scene)
floorHexa_01_10.addComponentOrReplace(gltfShape_2)
const transform_12 = new Transform({
  position: new Vector3(13, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_10.addComponentOrReplace(transform_12)
engine.addEntity(floorHexa_01_10)

const floorHexa_01_11 = new Entity()
floorHexa_01_11.setParent(scene)
floorHexa_01_11.addComponentOrReplace(gltfShape_2)
const transform_13 = new Transform({
  position: new Vector3(4, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_11.addComponentOrReplace(transform_13)
engine.addEntity(floorHexa_01_11)

const floorHexa_01_12 = new Entity()
floorHexa_01_12.setParent(scene)
floorHexa_01_12.addComponentOrReplace(gltfShape_2)
const transform_14 = new Transform({
  position: new Vector3(6, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_12.addComponentOrReplace(transform_14)
engine.addEntity(floorHexa_01_12)

const floorHexa_01_13 = new Entity()
floorHexa_01_13.setParent(scene)
floorHexa_01_13.addComponentOrReplace(gltfShape_2)
const transform_15 = new Transform({
  position: new Vector3(8, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_13.addComponentOrReplace(transform_15)
engine.addEntity(floorHexa_01_13)

const floorHexa_01_14 = new Entity()
floorHexa_01_14.setParent(scene)
floorHexa_01_14.addComponentOrReplace(gltfShape_2)
const transform_16 = new Transform({
  position: new Vector3(10, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_14.addComponentOrReplace(transform_16)
engine.addEntity(floorHexa_01_14)

const floorHexa_01_15 = new Entity()
floorHexa_01_15.setParent(scene)
floorHexa_01_15.addComponentOrReplace(gltfShape_2)
const transform_17 = new Transform({
  position: new Vector3(12, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_15.addComponentOrReplace(transform_17)
engine.addEntity(floorHexa_01_15)

const floorHexa_01_16 = new Entity()
floorHexa_01_16.setParent(scene)
floorHexa_01_16.addComponentOrReplace(gltfShape_2)
const transform_18 = new Transform({
  position: new Vector3(14, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_16.addComponentOrReplace(transform_18)
engine.addEntity(floorHexa_01_16)

const floorHexa_01_17 = new Entity()
floorHexa_01_17.setParent(scene)
floorHexa_01_17.addComponentOrReplace(gltfShape_2)
const transform_19 = new Transform({
  position: new Vector3(5, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_17.addComponentOrReplace(transform_19)
engine.addEntity(floorHexa_01_17)

const floorHexa_01_18 = new Entity()
floorHexa_01_18.setParent(scene)
floorHexa_01_18.addComponentOrReplace(gltfShape_2)
const transform_20 = new Transform({
  position: new Vector3(11, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_18.addComponentOrReplace(transform_20)
engine.addEntity(floorHexa_01_18)

const floorHexa_01_19 = new Entity()
floorHexa_01_19.setParent(scene)
floorHexa_01_19.addComponentOrReplace(gltfShape_2)
const transform_21 = new Transform({
  position: new Vector3(11, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_19.addComponentOrReplace(transform_21)
engine.addEntity(floorHexa_01_19)

const floorHexa_01_20 = new Entity()
floorHexa_01_20.setParent(scene)
floorHexa_01_20.addComponentOrReplace(gltfShape_2)
const transform_22 = new Transform({
  position: new Vector3(9, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_20.addComponentOrReplace(transform_22)
engine.addEntity(floorHexa_01_20)

const floorHexa_01_21 = new Entity()
floorHexa_01_21.setParent(scene)
floorHexa_01_21.addComponentOrReplace(gltfShape_2)
const transform_23 = new Transform({
  position: new Vector3(7, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_21.addComponentOrReplace(transform_23)
engine.addEntity(floorHexa_01_21)

const floorHexa_01_22 = new Entity()
floorHexa_01_22.setParent(scene)
floorHexa_01_22.addComponentOrReplace(gltfShape_2)
const transform_24 = new Transform({
  position: new Vector3(10, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_22.addComponentOrReplace(transform_24)
engine.addEntity(floorHexa_01_22)

const floorHexa_01_23 = new Entity()
floorHexa_01_23.setParent(scene)
floorHexa_01_23.addComponentOrReplace(gltfShape_2)
const transform_25 = new Transform({
  position: new Vector3(3, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_23.addComponentOrReplace(transform_25)
engine.addEntity(floorHexa_01_23)

const floorHexa_01_24 = new Entity()
floorHexa_01_24.setParent(scene)
floorHexa_01_24.addComponentOrReplace(gltfShape_2)
const transform_26 = new Transform({
  position: new Vector3(4, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_24.addComponentOrReplace(transform_26)
engine.addEntity(floorHexa_01_24)

const floorHexa_01_25 = new Entity()
floorHexa_01_25.setParent(scene)
floorHexa_01_25.addComponentOrReplace(gltfShape_2)
const transform_27 = new Transform({
  position: new Vector3(6, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_25.addComponentOrReplace(transform_27)
engine.addEntity(floorHexa_01_25)

const floorHexa_01_26 = new Entity()
floorHexa_01_26.setParent(scene)
floorHexa_01_26.addComponentOrReplace(gltfShape_2)
const transform_28 = new Transform({
  position: new Vector3(8, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_26.addComponentOrReplace(transform_28)
engine.addEntity(floorHexa_01_26)

const floorHexa_01_27 = new Entity()
floorHexa_01_27.setParent(scene)
floorHexa_01_27.addComponentOrReplace(gltfShape_2)
const transform_29 = new Transform({
  position: new Vector3(12, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_27.addComponentOrReplace(transform_29)
engine.addEntity(floorHexa_01_27)

const floorHexa_01_28 = new Entity()
floorHexa_01_28.setParent(scene)
floorHexa_01_28.addComponentOrReplace(gltfShape_2)
const transform_30 = new Transform({
  position: new Vector3(12, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_28.addComponentOrReplace(transform_30)
engine.addEntity(floorHexa_01_28)

const floorHexa_01_29 = new Entity()
floorHexa_01_29.setParent(scene)
floorHexa_01_29.addComponentOrReplace(gltfShape_2)
const transform_31 = new Transform({
  position: new Vector3(5, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_29.addComponentOrReplace(transform_31)
engine.addEntity(floorHexa_01_29)

const floorHexa_01_30 = new Entity()
floorHexa_01_30.setParent(scene)
floorHexa_01_30.addComponentOrReplace(gltfShape_2)
const transform_32 = new Transform({
  position: new Vector3(11, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_30.addComponentOrReplace(transform_32)
engine.addEntity(floorHexa_01_30)

const floorHexa_01_31 = new Entity()
floorHexa_01_31.setParent(scene)
floorHexa_01_31.addComponentOrReplace(gltfShape_2)
const transform_33 = new Transform({
  position: new Vector3(9, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_31.addComponentOrReplace(transform_33)
engine.addEntity(floorHexa_01_31)

const floorHexa_01_32 = new Entity()
floorHexa_01_32.setParent(scene)
floorHexa_01_32.addComponentOrReplace(gltfShape_2)
const transform_34 = new Transform({
  position: new Vector3(7, 0, 7),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_32.addComponentOrReplace(transform_34)
engine.addEntity(floorHexa_01_32)

const floorHexa_01_33 = new Entity()
floorHexa_01_33.setParent(scene)
floorHexa_01_33.addComponentOrReplace(gltfShape_2)
const transform_35 = new Transform({
  position: new Vector3(8, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_33.addComponentOrReplace(transform_35)
engine.addEntity(floorHexa_01_33)

const floorHexa_01_34 = new Entity()
floorHexa_01_34.setParent(scene)
floorHexa_01_34.addComponentOrReplace(gltfShape_2)
const transform_36 = new Transform({
  position: new Vector3(6, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_34.addComponentOrReplace(transform_36)
engine.addEntity(floorHexa_01_34)

const floorHexa_01_35 = new Entity()
floorHexa_01_35.setParent(scene)
floorHexa_01_35.addComponentOrReplace(gltfShape_2)
const transform_37 = new Transform({
  position: new Vector3(8, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_35.addComponentOrReplace(transform_37)
engine.addEntity(floorHexa_01_35)

const floorHexa_01_36 = new Entity()
floorHexa_01_36.setParent(scene)
floorHexa_01_36.addComponentOrReplace(gltfShape_2)
const transform_38 = new Transform({
  position: new Vector3(10, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_36.addComponentOrReplace(transform_38)
engine.addEntity(floorHexa_01_36)

const floorHexa_01_37 = new Entity()
floorHexa_01_37.setParent(scene)
floorHexa_01_37.addComponentOrReplace(gltfShape_2)
const transform_39 = new Transform({
  position: new Vector3(9, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_37.addComponentOrReplace(transform_39)
engine.addEntity(floorHexa_01_37)

const floorHexa_01_38 = new Entity()
floorHexa_01_38.setParent(scene)
floorHexa_01_38.addComponentOrReplace(gltfShape_2)
const transform_40 = new Transform({
  position: new Vector3(7, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorHexa_01_38.addComponentOrReplace(transform_40)
engine.addEntity(floorHexa_01_38)

const computer_01 = new Entity()
computer_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/Computer_01/Computer_01.glb')
computer_01.addComponentOrReplace(gltfShape_3)
const transform_41 = new Transform({
  position: new Vector3(8, 0, 6),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
computer_01.addComponentOrReplace(transform_41)
engine.addEntity(computer_01)
/*
const lightCylinder_01 = new Entity()
lightCylinder_01.setParent(scene)
const gltfShape_4 = new GLTFShape('models/LightCylinder_01/LightCylinder_01.glb')
lightCylinder_01.addComponentOrReplace(gltfShape_4)
const transform_42 = new Transform({
  position: new Vector3(8, 0, 4.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(3, 12.5, 3.5)
})
lightCylinder_01.addComponentOrReplace(transform_42)
engine.addEntity(lightCylinder_01)
*/
const streetFence_01 = new Entity()
streetFence_01.setParent(scene)
const gltfShape_5 = new GLTFShape('models/StreetFence_01/StreetFence_01.glb')
streetFence_01.addComponentOrReplace(gltfShape_5)
const transform_43 = new Transform({
  position: new Vector3(12, 0, 7.5),
  rotation: new Quaternion(0, 0.4713967368259979, 0, 0.8819212643483552),
  scale: new Vector3(5, 0.5, 0.5)
})
streetFence_01.addComponentOrReplace(transform_43)
engine.addEntity(streetFence_01)

const streetFence_01_2 = new Entity()
streetFence_01_2.setParent(scene)
streetFence_01_2.addComponentOrReplace(gltfShape_5)
const transform_44 = new Transform({
  position: new Vector3(4, 0, 7.5),
  rotation: new Quaternion(0, 0.8819212643483549, 0, 0.4713967368259978),
  scale: new Vector3(5, 0.5, 0.5)
})
streetFence_01_2.addComponentOrReplace(transform_44)
engine.addEntity(streetFence_01_2)

const streetFence_01_3 = new Entity()
streetFence_01_3.setParent(scene)
streetFence_01_3.addComponentOrReplace(gltfShape_5)
const transform_45 = new Transform({
  position: new Vector3(8.5, 0, 0.1606328264900272),
  rotation: new Quaternion(0, 1.942890293094024e-16, 0, 1.0000000000000004),
  scale: new Vector3(5, 0.5, 0.5)
})
streetFence_01_3.addComponentOrReplace(transform_45)
engine.addEntity(streetFence_01_3)

const hallway_Module_StraightHalf_01 = new Entity()
hallway_Module_StraightHalf_01.setParent(scene)
const gltfShape_6 = new GLTFShape('models/Hallway_Module_StraightHalf_01/Hallway_Module_StraightHalf_01.glb')
hallway_Module_StraightHalf_01.addComponentOrReplace(gltfShape_6)
const transform_46 = new Transform({
  position: new Vector3(8, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
hallway_Module_StraightHalf_01.addComponentOrReplace(transform_46)
engine.addEntity(hallway_Module_StraightHalf_01)

const floorSciFiPanel_04 = new Entity()
floorSciFiPanel_04.setParent(scene)
const gltfShape_7 = new GLTFShape('models/FloorSciFiPanel_04/FloorSciFiPanel_04.glb')
floorSciFiPanel_04.addComponentOrReplace(gltfShape_7)
const transform_47 = new Transform({
  position: new Vector3(10.561344229145437, 0, 16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(2.5, 1, 3.5)
})
floorSciFiPanel_04.addComponentOrReplace(transform_47)
engine.addEntity(floorSciFiPanel_04)

const floorHexa_02 = new Entity()
floorHexa_02.setParent(scene)
const gltfShape_8 = new GLTFShape('models/FloorHexa_02/FloorHexa_02.glb')
floorHexa_02.addComponentOrReplace(gltfShape_8)
const transform_48 = new Transform({
  position: new Vector3(10.5, 0, 13.452071902531603),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 2.5393973462025023)
})
floorHexa_02.addComponentOrReplace(transform_48)
engine.addEntity(floorHexa_02)

const floorHexa_02_2 = new Entity()
floorHexa_02_2.setParent(scene)
floorHexa_02_2.addComponentOrReplace(gltfShape_8)
const transform_49 = new Transform({
  position: new Vector3(5.5689372997168, 0, 13.44061780526693),
  rotation: new Quaternion(0, -1.0000000000000004, 0, 3.0531133177191805e-16),
  scale: new Vector3(1, 1, 2.5393973462025023)
})
floorHexa_02_2.addComponentOrReplace(transform_49)
engine.addEntity(floorHexa_02_2)